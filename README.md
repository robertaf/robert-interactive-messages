# Message Menus API Robert.ai

## Setup

### Start application
Run `npm start` in directory's folder

### Run ngrok
On a different terminal window run `ngrok http 3000`
p.s: ngrok is not installed globally as a default, so you must run the command in the same folder ngrok was installed or attempt to symlink it.

### Update both event subscriptions and interactive components Request URL 

Event subscriptions https://api.slack.com/apps/A8R37UP53/event-subscriptions
`NGROK_HTTPS_URL/slack/events`

Interactive components https://api.slack.com/apps/A8R37UP53/interactive-messages
Request URL:
`NGROK_HTTPS_URL/slack/actions`

Options Load URL (for Message Menus):
not needed now

## Usage

DM @El Roberto and he'll answer you with an interactive menu. Good luck!
