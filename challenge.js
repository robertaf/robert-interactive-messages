const express = require('express')
const bodyParser = require('body-parser')
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/slack/events', (req, res) => {
	res.send(req.body.challenge)
});

app.get('/health', (req, res) => {
	res.send(new Date)
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))