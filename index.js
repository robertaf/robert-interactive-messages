require('dotenv').config();

const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const slackEventsAPI = require('@slack/events-api');
const slackInteractiveMessages = require('@slack/interactive-messages');
const normalizePort = require('normalize-port');
const cloneDeep = require('lodash.clonedeep');
const bot = require('./lib/bot');
const axios = require('axios');
const moment = require('moment');

// const Sequelize = require('sequelize');

// const sequelize = new Sequelize(process.env.MYSQL_DB, process.env.MYSQL_USER, process.env.MYSQL_PASS, {
//     host: process.env.MYSQL_HOST,
//     dialect: 'mysql',
//     pool: {
//         max: 500,
//         min: 10,
//         idle: 5000,
//         acquire: 5000,
//         evict: 10000,
//         handleDisconnects: true
//     },
//     retry:{max: 3},
//     logging: false,
//     dialectOptions: {
//                 requestTimeout: 0,
//     },
//     define: {
//       timestamps: false
//     }
// });

// const reportJobs = sequelize.define('report_jobs', {
//     account_id: { type: Sequelize.STRING, allowNull: true },
//     start_date: Sequelize.STRING,
//     end_date: { type: Sequelize.STRING, allowNull: true },
//     report_type: { type: Sequelize.STRING, allowNull: true }, // pending, answered, completed
//     file_name: { type: Sequelize.STRING, allowNull: true },
//     params: { type: Sequelize.JSON, allowNull: true }, // json with all job params from watson
//     status: { type: Sequelize.STRING, allowNull: true },
//     result: { type: Sequelize.STRING, allowNull: true },
//     created_at: { type: Sequelize.STRING, allowNull: true }
// });

// const people = sequelize.define('reamp_people', {
//     username: { type: Sequelize.STRING, allowNull: true },
//     userid: Sequelize.STRING,
//     rmcId: {type: Sequelize.STRING, allowNull: true},
//     real_name: { type: Sequelize.STRING, allowNull: true },
//     nick_name: { type: Sequelize.STRING, allowNull: true },
//     email: { type: Sequelize.STRING, allowNull: true },
//     area: { type: Sequelize.STRING, allowNull: true }, // opec, midia e audiencia, tecnologia, bi, administrativo
//     cargo: { type: Sequelize.STRING, allowNull: true },
//     ramal: { type: Sequelize.STRING, allowNull: true },
//     status_cadastro: { type: Sequelize.STRING, allowNull: true }, // esperando cadastro, cadastrada
//     params: { type: Sequelize.TEXT, allowNull: true }
// });

// sequelize.sync().then(function (err) {
//     // console.log(err);
//     if (err) {
//         console.log("err sequelize.sync");
//     } else {
//         console.log('Item table created successfully');
//     }
// });

let reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzEyfQ.S5zBc662Hrw2ik6149SnGcd7tCYgS8ElLDAZDzeiqUk';

// --- Slack Events ---
const slackEvents = slackEventsAPI.createSlackEventAdapter(process.env.SLACK_VERIFICATION_TOKEN);
// --- Slack Interactive Messages ---
const slackMessages =
  slackInteractiveMessages.createMessageAdapter(process.env.SLACK_VERIFICATION_TOKEN);

// carregar contas cadastradas no rmc
let accounts = [];
let accountId;
let selectedCampaigns = {
  "campaigns":[],
  "load-partitions":"1"
};
let startDay, startMonth, startYear, startDate, endDate, endDay, endMonth, endYear;

axios.get('http://nemesis.reamp.com.br/v1/accounts', { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } })
.then(function(response){
  if(response){
    response.data.data.map((item) => {
      accounts.push({
        "text": item.attributes.name,
        "value": item.id
      })
    });
  }
}).catch(error => {
  console.log(error);
});

slackEvents.on('team_join', (event) => {
  bot.introduceToUser(event.user.id);
});

slackEvents.on('message', (event) => {
  let historyTS = moment.unix(event.ts).format("dddd, MMMM Do YYYY, h:mm:ss a");
  let now = moment(Date.now()).format("dddd, MMMM Do YYYY, h:mm:ss a");
  // console.log("current ts: ", now);

  // Filter out messages from this bot itself or updates to messages
  if (event.subtype === 'bot_message' || event.subtype === 'message_changed') {
    return;
  }
  bot.handleDirectMessage(event);
});

// Helper functions

function findAttachment(message, actionCallbackId) {
  return message.attachments.find(a => a.callback_id === actionCallbackId);
}

function acknowledgeActionFromMessage(originalMessage, actionCallbackId, ackText) {
  const message = cloneDeep(originalMessage);
  const attachment = findAttachment(message, actionCallbackId);
  delete attachment.actions;
  attachment.text = `:white_check_mark: ${ackText}`;
  return message;
}

function findSelectedOption(originalMessage, actionCallbackId, selectedValue) {
  const attachment = findAttachment(originalMessage, actionCallbackId);

  try {
    return attachment.actions[0].options.find(o => o.value === selectedValue);
  } catch(e) {
    console.log("erro na hora de retornar o attachment actions");
    console.log(e);
    return null;
  }
}

function getCampaigns(accountId) {
  return new Promise((resolve, reject) => {
       axios.get(`http://nemesis.reamp.com.br/v1/accounts/${accountId}/campaigns`, { headers: { 'Authorization': 'Bearer' + reampLoginToken, "CONTENT_TYPE": "application/json" } })
      .then((response) =>{
        if(response){
          let campaigns = response.data.data.map((item) => {
              return({
                "text": item.attributes.name,
                "value": item.id
              });
            });
          resolve(campaigns);
        }
      });
  })
}

// Action handling

slackMessages.action('order:start', (payload, respond) => {
  // Create an updated message that acknowledges the user's action (even if the result of that
  // action is not yet complete).
  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:start',
                                                      'Tirando um invoice para você.');

  bot.selectStartDate(payload.user.id, accounts)
    .then(respond)
    .catch(console.error);

  // The updated message is returned synchronously in response
  return updatedMessage;
});

slackMessages.action('order:select_start_date', (payload, respond) => {
  console.log(payload.actions[0])
  //pegar dia
  if(payload.actions[0].name == 'day'){
    startDay = payload.actions[0].selected_options[0].value;
  }

  //pegar mês
  if(payload.actions[0].name == 'month'){
    startMonth = payload.actions[0].selected_options[0].value;
  }

  //pegar ano
  if(payload.actions[0].name == 'year'){
    startYear = payload.actions[0].selected_options[0].value;
  }

  if(startDay && startMonth && startYear){
    const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:select_start_date',
                                                      'Data início.');
    startDate = moment().year(startYear).month(startMonth).date(startDay).format("YYYY-MM-DD");
    bot.selectEndDate()
      .then(respond)
      .catch(console.error);

    return updatedMessage;
  }
});

slackMessages.action('order:select_end_date', (payload, respond) => {
  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:select_end_date',
                                                      'Data fim');

  if(payload.actions[0].name == 'day'){
    endDay = payload.actions[0].selected_options[0].value;
  }

  if(payload.actions[0].name == 'month'){
    endMonth = payload.actions[0].selected_options[0].value;
  }

  if(payload.actions[0].name == 'year'){
    endYear = payload.actions[0].selected_options[0].value;
  }

  if(endDay && endMonth && endYear){
    endDate = moment().year(endYear).month(endMonth).date(endDay).format("YYYY-MM-DD");
    console.log(startDate, endDate)
    // Start an order, and when that completes, send another message to the user.
    bot.startOrder(payload.user.id, accounts)
      .then(respond)
      .catch(console.error);

      return updatedMessage;
  }
});

slackMessages.action('order:select_type', (payload, respond) => {
  const selectedType = findSelectedOption(payload.original_message, 'order:select_type', payload.actions[0].selected_options[0].value);
  accountId = payload.actions[0].selected_options[0].value;
  
  //filtrar contas pra retornar nome da conta selecionada, usando o accountId
  let accountName = accounts.filter(account => account.value == selectedType.value);

  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:select_type',
                                                      `Conta selecionada: ${accountName[0].text}`);

  getCampaigns(accountId).then(campaigns => {
    bot.optionSelectionForOrder(payload.user.id, campaigns)
    .then((response) => {
      // Keep the context from the updated message but use the new text and attachment
      updatedMessage.text = response.text;
      if (response.attachments && response.attachments.length > 0) {
        updatedMessage.attachments.push(response.attachments[0]);
      }
      return updatedMessage;
    })
    .then(respond)
    .catch(error => {
      console.log(error);
    });
      return updatedMessage;
  });
  return updatedMessage;
  
});

slackMessages.action('order:select_option', (payload, respond) => {
  const optionName = payload.actions[0].name; //select_campaign
  const selectedChoice = findSelectedOption(payload.original_message, 'order:select_option', payload.actions[0].selected_options[0].value); //campanha selecionada
  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:select_option',
                                                      `Campanha selecionada: ${selectedChoice.text}`); //mensagem que deveria aparecer após selecionar a campanha
  let campaignId = selectedChoice.value;
  selectedCampaigns.campaigns.push(selectedChoice.value);

  getCampaigns(accountId).then(campaigns => {
    //parei aqui! preciso filtrar campaigns e tirar as selectedCampaigns da lista
    
    const filteredCampaigns = campaigns.filter(campaign => !selectedCampaigns.campaigns.includes(campaign.value))

    bot.optionSelectionForOverlap(payload.user.id, filteredCampaigns)
    .then((response) => {
      // Keep the context from the updated message but use the new text and attachment
      updatedMessage.text = response.text;
      if (response.attachments && response.attachments.length > 0) {
        updatedMessage.attachments.push(response.attachments[0]);
      }
      return updatedMessage;
    })
    .then(respond)
    .catch(error => {
      console.log(error);
    });
  });

});

slackMessages.action('order:select_second_option', (payload, respond) => {
  const optionName = payload.actions[0].name; //select_campaign
  const selectedChoice = findSelectedOption(payload.original_message, 'order:select_second_option', payload.actions[0].selected_options[0].value); //campanha selecionada
  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:select_second_option',
                                                      `Campanha selecionada: ${selectedChoice.text}`); //mensagem que deveria aparecer após selecionar a campanha

  // let campaignId = selectedChoice.value;
  // let unfilteredCampaigns = payload.original_message.attachments[1].actions[0].options;
  // let filteredCampaigns = unfilteredCampaigns.filter(campaign => campaign.value !== campaignId);

  selectedCampaigns.campaigns.push(selectedChoice.value);
  // console.log("selected: ", selectedCampaigns);

  bot.optionButtonForOverlapOne(payload.user.id)
  .then((response) => {
    // Keep the context from the updated message but use the new text and attachment
    updatedMessage.text = response.text;
    if (response.attachments && response.attachments.length > 0) {
      updatedMessage.attachments.push(response.attachments[0]);
    }
    return updatedMessage;
  })
  .then(respond)
  .catch(error => {
    console.log(error);
  });
});

slackMessages.action('order:select_third_option', (payload, respond) => {
  console.log("1")
  const optionName = payload.actions[0].name; //select_campaign
  const selectedChoice = findSelectedOption(payload.original_message, 'order:select_third_option', payload.actions[0].selected_options[0].value); //campanha selecionada
  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:select_third_option',
                                                      `Campanha selecionada: ${selectedChoice.text}`); //mensagem que deveria aparecer após selecionar a campanha

  // let campaignId = selectedChoice.value;
  // let unfilteredCampaigns = payload.original_message.attachments[1].actions[0].options;
  // let filteredCampaigns = unfilteredCampaigns.filter(campaign => campaign.value !== campaignId);

  selectedCampaigns.campaigns.push(selectedChoice.value);
  // console.log("selected: ", selectedCampaigns);

  bot.optionButtonForOverlapTwo(payload.user.id)
  .then((response) => {
    // Keep the context from the updated message but use the new text and attachment
    updatedMessage.text = response.text;
    if (response.attachments && response.attachments.length > 0) {
      updatedMessage.attachments.push(response.attachments[0]);
    }
    return updatedMessage;
  })
  .then(respond)
  .catch(error => {
    console.log(error);
  });
});

slackMessages.action('order:select_fourth_option', (payload, respond) => {
  console.log("2")
  const optionName = payload.actions[0].name; //select_campaign
  const selectedChoice = findSelectedOption(payload.original_message, 'order:select_fourth_option', payload.actions[0].selected_options[0].value); //campanha selecionada
  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:select_fourth_option',
                                                      `Campanha selecionada: ${selectedChoice.text}`); //mensagem que deveria aparecer após selecionar a campanha

  // let campaignId = selectedChoice.value;
  // let unfilteredCampaigns = payload.original_message.attachments[1].actions[0].options;
  // let filteredCampaigns = unfilteredCampaigns.filter(campaign => campaign.value !== campaignId);

  selectedCampaigns.campaigns.push(selectedChoice.value);
  // console.log("selected: ", selectedCampaigns);

  bot.optionButtonForOverlapThree(payload.user.id)
  .then((response) => {
    // Keep the context from the updated message but use the new text and attachment
    updatedMessage.text = response.text;
    if (response.attachments && response.attachments.length > 0) {
      updatedMessage.attachments.push(response.attachments[0]);
    }
    return updatedMessage;
  })
  .then(respond)
  .catch(error => {
    console.log(error);
  });
});


slackMessages.action('order:first_button_add_campaign', (payload, respond) => {
  console.log("3")
  let button = payload.actions[0].value;

  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:first_button_add_campaign',
                                                      'Não sei o que escrever aqui ainda:');

  console.log(button);
  if(button == "order:yes"){
    getCampaigns(accountId).then(campaigns => {
     
      const filteredCampaigns = campaigns.filter(campaign => !selectedCampaigns.campaigns.includes(campaign.value))

      bot.optionSelectionForThirdOption(payload.user.id, filteredCampaigns)
      .then((response) => {
        // Keep the context from the updated message but use the new text and attachment
        updatedMessage.text = response.text;
        if (response.attachments && response.attachments.length > 0) {
          updatedMessage.attachments.push(response.attachments[0]);
        }
        return updatedMessage;
      })
      .then(respond)
      .catch(error => {
        console.log(error);
      });
    });
  } else if (button == "order:no"){
    // reportJobs.create({
    //   "account_id": "1253",
    //   "start_date": "",
    //   "end_date": "",
    //   "report_type": "report-overlap-campaign",
    //   "file_name": "",
    //   "params": selectedCampaigns,
    //   "status": "teste",
    //   "result": ""
    // })
  } else {
    console.log("deu ruim")
  }
});

slackMessages.action('order:second_button_add_campaign', (payload, respond) => {
  console.log("4")
  let button = payload.actions[0].value;

  const updatedMessage = acknowledgeActionFromMessage(payload.original_message, 'order:second_button_add_campaign',
                                                      'Não sei o que escrever aqui ainda:');

  if(button == "order:yes"){
    getCampaigns(accountId).then(campaigns => {      
      const filteredCampaigns = campaigns.filter(campaign => !selectedCampaigns.campaigns.includes(campaign.value))

      bot.optionSelectionForFourthOption(payload.user.id, filteredCampaigns)
      .then((response) => {
        // Keep the context from the updated message but use the new text and attachment
        updatedMessage.text = response.text;
        if (response.attachments && response.attachments.length > 0) {
          updatedMessage.attachments.push(response.attachments[0]);
        }
        return updatedMessage;
      })
      .then(respond)
      .catch(error => {
        console.log(error);
      });
    });
  } else if (button == "order:no"){
    // reportJobs.create({
    //   "account_id": "1253",
    //   "start_date": "",
    //   "end_date": "",
    //   "report_type": "report-overlap-campaign",
    //   "file_name": "",
    //   "params": selectedCampaigns,
    //   "status": "teste",
    //   "result": ""
    // })
  } else {
    console.log("deu ruim")
  }
});

slackMessages.action('order:select_campaign_failed', (payload, respond) => {
  //handle no campaigns
});

// Create the server
const port = normalizePort(process.env.PORT || '3000');
const app = express();
app.use(bodyParser.json());
app.use('/slack/events', slackEvents.expressMiddleware());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/slack/actions', slackMessages.expressMiddleware());
// Start the server
http.createServer(app).listen(port, () => {
  console.log(`server listening on port ${port}`);
});
