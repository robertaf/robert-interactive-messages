const { WebClient } = require('@slack/client');
const menu = require('./menu');
const map = require('lodash.map');
const axios = require('axios');

let reampLoginToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxMzEyfQ.S5zBc662Hrw2ik6149SnGcd7tCYgS8ElLDAZDzeiqUk';

// Helper functions
function nextOptionForOrder(order) {
  const item = menu.items.find(i => i.id === order.type);
  if (!item) {
    throw new Error('This menu item was not found.');
  }
  return item.options.find(o => !Object.prototype.hasOwnProperty.call(order.options, o));
}

function orderIsComplete(order) {
  return !nextOptionForOrder(order);
}

function summarizeOrder(order) {
  const item = menu.items.find(i => i.id === order.type);
  let summary = item.name;
  const optionsText = map(order.options, (choice, optionName) => `${choice} ${optionName}`);
  if (optionsText.length !== 0) {
    summary += ` with ${optionsText.join(' and ')}`;
  }
  return summary.toLowerCase();
}

function capitalizeFirstChar(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

// Bot
// TODO: remove
const slackClientOptions = {};
if (process.env.SLACK_ENV) {
  slackClientOptions.slackAPIUrl = process.env.SLACK_ENV;
}
const bot = {
  web: new WebClient(process.env.SLACK_BOT_TOKEN, slackClientOptions),
  orders: {},

  introduceToUser(userId) {
    this.web.im.open(userId)
      .then(resp => this.web.chat.postMessage(resp.channel.id, 'Prazer, sou o Robert Bot! :robot_face:', {
        attachments: [
          {
            color: '#5A352D',
            title: 'Como posso te ajudar?',
            callback_id: 'order:start',
            actions: [
              {
                name: 'start',
                text: 'Tirar invoice',
                type: 'button',
                value: 'order:start',
              },
            ],
          },
        ],
      }))
      .catch(console.error);
  },

  startOrder(userId, accounts) {
    // TODO: error handling
    if (this.orders[userId]) {
      return Promise.resolve({
        text: 'Ainda estou processando, um segundo',
        replace_original: false,
      });
    }

    // Initialize the order
    this.orders[userId] = {
      options: {},
    };

    return Promise.resolve({
      text: 'Qual a conta?',
      attachments: [
        {
          color: '#5A352D',
          callback_id: 'order:select_type',
          text: '', // attachments must have text property defined (abstractable)
          actions: [
            {
              name: 'select_account',
              type: 'select',
              options: accounts,
            },
          ],
        },
      ],
    });
  },

  selectTypeForOrder(userId, accountId, campaigns) {
    const order = this.orders[userId];
    // TODO: error handling
    if (!order) {
      return Promise.resolve({
        text: 'I cannot find that order. Message me to start a new order.',
        replace_original: false,
      });
    }

    // TODO: validation?
    order.type = accountId;

    if (campaigns) {
      return this.optionSelectionForOrder(userId, campaigns);
    }
    return this.finishOrder(userId);
  },

  selectStartDate() {
    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
          {
            color: '#5A352D',
            title: 'Qual a data de início do seu relatório?',
            callback_id: 'order:select_start_date',
            actions: [
              {
                name: 'day',
                type: 'select',
                options: menu.listOfDays(),
              },
              {
                name: 'month',
                type: 'select',
                options: menu.listOfMonths(),
              },
              {
                name: 'year',
                type: 'select',
                options: menu.listOfYears(),
              },
            ],
          },
        ],
    })
  },

  selectEndDate() {
    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
          {
            color: '#5A352D',
            title: 'Qual a data final do seu relatório?',
            callback_id: 'order:select_end_date',
            actions: [
              {
                name: 'day',
                type: 'select',
                options: menu.listOfDays(),
              },
              {
                name: 'month',
                type: 'select',
                options: menu.listOfMonths(),
              },
              {
                name: 'year',
                type: 'select',
                options: menu.listOfYears(),
              },
            ],
          },
        ],
    })
  },

  optionSelectionForOrder(userId, campaigns) {
    const order = this.orders[userId];
    // TODO: what happens if this throws?

    // console.log(campaigns);
    if(campaigns.length < 1){
      return Promise.resolve({
        callback_id: 'order:select_campaign_failed',
        text: 'Conta não possui campanhas cadastradas'
      });
    }

    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
        {
          color: '#5A352D',
          callback_id: 'order:select_option',
          text: `Qual a campanha?`,
          actions: [
            {
              name: 'select_campaign',
              type: 'select',
              options: campaigns,
            },
          ],
        },
      ],
    });
  },

  optionSelectionForOverlap(userId, campaigns) {
    const order = this.orders[userId];
    // TODO: what happens if this throws?

    if(campaigns.length < 1){
      return Promise.resolve({
        callback_id: 'order:select_campaign_failed',
        text: 'Conta não possui campanhas cadastradas'
      });
    }

    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
        {
          color: '#5A352D',
          callback_id: 'order:select_second_option',
          text: `Qual a campanha?`,
          actions: [
            {
              name: 'select_second_option',
              type: 'select',
              options: campaigns,
            },
          ],
        },
      ],
    });
  },

  optionSelectionForThirdOption(userId, campaigns) {
    const order = this.orders[userId];
    // TODO: what happens if this throws?

    if(campaigns.length < 1){
      return Promise.resolve({
        callback_id: 'order:select_campaign_failed',
        text: 'Conta não possui campanhas cadastradas'
      });
    }

    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
        {
          color: '#5A352D',
          callback_id: 'order:select_third_option',
          text: `Qual a campanha?`,
          actions: [
            {
              name: 'select_third_option',
              type: 'select',
              options: campaigns,
            },
          ],
        },
      ],
    });
  },

  optionSelectionForFourthOption(userId, campaigns) {
    const order = this.orders[userId];
    // TODO: what happens if this throws?

    if(campaigns.length < 1){
      return Promise.resolve({
        callback_id: 'order:select_campaign_failed',
        text: 'Conta não possui campanhas cadastradas'
      });
    }
    console.log("3")

    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
        {
          color: '#5A352D',
          callback_id: 'order:select_fourth_option',
          text: `Qual a campanha?`,
          actions: [
            {
              name: 'select_fourth_option',
              type: 'select',
              options: campaigns,
            },
          ],
        },
      ],
    });
  },

  optionButtonForOverlapOne(userId) {
    const order = this.orders[userId];
    //crtl z aqui pra voltar aos botões mas o select deu bom
    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
          {
            color: '#5A352D',
            title: 'Adicionar nova campanha para overlap?',
            callback_id: 'order:first_button_add_campaign',
            actions: [
              {
                name: 'yes',
                text: 'Sim',
                value: 'order:yes',
                type: 'button'
              },
              {
                name: 'no',
                text: 'Não',
                value: 'order:no',
                type: 'button'
              },
            ],
          },
        ],
    });
  },

  optionButtonForOverlapTwo(userId) {
    const order = this.orders[userId];
    //crtl z aqui pra voltar aos botões mas o select deu bom
    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
          {
            color: '#5A352D',
            title: 'Adicionar nova campanha para overlap?',
            callback_id: 'order:second_button_add_campaign',
            actions: [
              {
                name: 'yes',
                text: 'Sim',
                value: 'order:yes',
                type: 'button'
              },
              {
                name: 'no',
                text: 'Não',
                value: 'order:no',
                type: 'button'
              },
            ],
          },
        ],
    });
  },

  optionButtonForOverlapThree(userId) {
    const order = this.orders[userId];
    //crtl z aqui pra voltar aos botões mas o select deu bom
    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
          {
            color: '#5A352D',
            title: 'Adicionar nova campanha para overlap?',
            callback_id: 'order:third_button_add_campaign',
            actions: [
              {
                name: 'yes',
                text: 'Sim',
                value: 'order:yes',
                type: 'button'
              },
              {
                name: 'no',
                text: 'Não',
                value: 'order:no',
                type: 'button'
              },
            ],
          },
        ],
    });
  },

  optionButtonForOverlapFour(userId) {
    const order = this.orders[userId];
    //crtl z aqui pra voltar aos botões mas o select deu bom
    return Promise.resolve({
      text: 'Trabalhando no seu invoice',
      attachments: [
          {
            color: '#5A352D',
            title: 'Adicionar nova campanha para overlap?',
            callback_id: 'order:third_button_add_campaign',
            actions: [
              {
                name: 'yes',
                text: 'Sim',
                value: 'order:yes',
                type: 'button'
              },
              {
                name: 'no',
                text: 'Não',
                value: 'order:no',
                type: 'button'
              },
            ],
          },
        ],
    });
  },

  selectOptionForOrder(userId, optionId, optionValue) {
    const order = this.orders[userId];

    // TODO: error handling
    if (!order) {
      return Promise.resolve({
        text: 'I cannot find that order. Message me to start a new order.',
        replace_original: false,
      });
    }

    // TODO: validation?
    order.options[optionId] = optionValue;

    if (!orderIsComplete(order)) {
      return this.optionSelectionForOrder(userId);
    }
    return this.finishOrder(userId);
  },

  // TODO: error handling
  finishOrder(userId) {
    const order = this.orders[userId];
    const item = menu.items.find(i => i.id === order.type);
    let fields = [
      {
        title: 'Drink',
        value: item.name,
      },
    ];
    fields = fields.concat(map(order.options, (choiceId, optionId) => {
      const choiceName = menu.choiceNameForId(optionId, choiceId);
      return { title: capitalizeFirstChar(optionId), value: choiceName };
    }));

    return axios.post(process.env.SLACK_WEBHOOK_URL, {
      text: `<@${userId}> has submitted a new coffee order.`,
      attachments: [
        {
          color: '#5A352D',
          title: 'Order details',
          text: summarizeOrder(order),
          fields,
        },
      ],
    }).then(() => Promise.resolve({
      text: `Your order of a ${summarizeOrder(order)} is coming right up!`,
    }));
  },

  handleDirectMessage(message) {
    if (!this.orders[message.user]) {
      this.introduceToUser(message.user);
    } else {
      this.web.chat.postMessage(message.channel, 'Vamos continuar trabalhando na sua requisição.')
        .catch(console.error);
    }
  },
};

module.exports = bot;
