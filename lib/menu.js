const menu = {
  days: [
    {
      id: '1',
      name: '1',
      options: ['1'],
    },
    {
      id: '2',
      name: '2',
      options: ['2'],
    },
    {
      id: '3',
      name: '3',
      options: ['3'],
    },
    {
      id: '4',
      name: '4',
      options: ['4'],
    },
    {
      id: '5',
      name: '5',
      options: ['5'],
    },
    {
      id: '6',
      name: '6',
      options: ['milk'],
    },
    {
      id: '7',
      name: '7',
      options: ['7'],
    },
    {
      id: '8',
      name: '8',
      options: ['8'],
    },
    {
      id: '9',
      name: '9',
      options: ['9'],
    },
    {
      id: '10',
      name: '10',
      options: ['10'],
    },
    {
      id: '11',
      name: '11',
      options: ['11'],
    },
    {
      id: '12',
      name: '12',
      options: ['12'],
    },
    {
      id: '13',
      name: '13',
      options: ['13'],
    },
    {
      id: '14',
      name: '14',
      options: ['14'],
    },
    {
      id: '15',
      name: '15',
      options: ['15'],
    },
    {
      id: '16',
      name: '16',
      options: ['16'],
    },
    {
      id: '17',
      name: '17',
      options: ['17'],
    },
    {
      id: '18',
      name: '18',
      options: ['18'],
    },
    {
      id: '19',
      name: '19',
      options: ['19'],
    },
    {
      id: '20',
      name: '20',
      options: ['20'],
    },
    {
      id: '21',
      name: '21',
      options: ['21'],
    },
    {
      id: '22',
      name: '22',
      options: ['22'],
    },
    {
      id: '23',
      name: '23',
      options: ['23'],
    },
    {
      id: '24',
      name: '24',
      options: ['24'],
    },
    {
      id: '25',
      name: '25',
      options: ['25'],
    },
    {
      id: '26',
      name: '26',
      options: ['26'],
    },
    {
      id: '27',
      name: '27',
      options: ['27'],
    },
    {
      id: '28',
      name: '28',
      options: ['28'],
    },
    {
      id: '29',
      name: '29',
      options: ['29'],
    },
    {
      id: '30',
      name: '30',
      options: ['30'],
    },
    {
      id: '31',
      name: '31',
      options: ['31'],
    },
  ],
  months: [
    {
      id:'jan',
      name:'january',
      options: ['january'],
    },
    {
      id:'fev',
      name:'february',
      options: ['february'],
    },
    {
      id:'mar',
      name:'march',
      options: ['march'],
    },
    {
      id:'abr',
      name:'april',
      options: ['april'],
    },
    {
      id:'mai',
      name:'may',
      options: ['may'],
    },
    {
      id:'jun',
      name:'june',
      options: ['june'],
    },
    {
      id:'jul',
      name:'july',
      options: ['july'],
    },
    {
      id:'ago',
      name:'august',
      options: ['august'],
    },
    {
      id:'set',
      name:'september',
      options: ['september'],
    },
    {
      id:'out',
      name:'october',
      options: ['october'],
    },
    {
      id:'nov',
      name:'november',
      options: ['november'],
    },
    {
      id:'dez',
      name:'december',
      options: ['december'],
    },
  ],
  years: [
    {
      id: '2018',
      name: '2018',
      options: ['2018']
    },
    {
      id: '2017',
      name: '2017',
      options: ['2017']
    },
    {
      id: '2016',
      name: '2016',
      options: ['2016']
    },
  ],
  options: [
    {
      id: 'strength',
      choices: [
        {
          id: 'single',
          name: 'Single',
        },
        {
          id: 'double',
          name: 'Double',
        },
        {
          id: 'triple',
          name: 'Triple',
        },
        {
          id: 'quad',
          name: 'Quad',
        },
      ],
    },
    {
      id: 'milk',
      choices: [
        {
          id: 'whole',
          name: 'Whole',
        },
        {
          id: 'lowfat',
          name: 'Low fat',
        },
        {
          id: 'almond',
          name: 'Almond',
        },
        {
          id: 'soy',
          name: 'Soy',
        },
      ],
    },
  ],

  listOfTypes() {
    return menu.items.map(i => ({ text: i.name, value: i.id }));
  },

  listOfDays() {
    return menu.days.map(i => ({ text: i.name, value: i.id }));
  },
  listOfMonths() {
    return menu.months.map(i => ({ text: i.name, value: i.id }));
  },
  listOfYears() {
    return menu.years.map(i => ({ text: i.name, value: i.id }));
  },

  listOfChoicesForOption(optionId) {
    return menu.options.find(o => o.id === optionId).choices
      .map(c => ({ text: c.name, value: c.id }));
  },

  choiceNameForId(optionId, choiceId) {
    const option = menu.options.find(o => o.id === optionId);
    if (option) {
      return option.choices.find(c => c.id === choiceId).name;
    }
    return false;
  },
};

module.exports = menu;
